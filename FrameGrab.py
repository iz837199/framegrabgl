from pypylon import pylon
import cv2
import time
import sys
from datetime import datetime
import configparser
import os


def builtin_image_capture(duration, wait, image_folder):
    # create a VideoCapture object
    cap = cv2.VideoCapture(0)
    # check if the webcam is open
    if not cap.isOpened():
        raise IOError("Webcam is not open")
    
    # set the resolution
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)

    # get the start time
    start_time = datetime.now()

    while (datetime.now() - start_time).total_seconds() < duration:
        print(datetime.now())
        
        # read a frame
        read_okay, frame = cap.read()

        # check the frame is correctly read
        if not read_okay:
            print("The frame has not been read correctly")

        # save the frame as an image file
        
        # get the current timestamp and format it as a string
        timestamp = datetime.now().strftime('%H%M%S_%f')
        # create the full file path
        file_path = os.path.join(image_folder, f"{timestamp}.png")
        
        # check if the directory exists and create it if necessary
        directory = os.path.dirname(file_path)
        if not os.path.exists(directory):
            os.makedirs(directory)
        
        cv2.imwrite(file_path, frame, [cv2.IMWRITE_PNG_COMPRESSION, 0])

        # wait for wait seconds before capturing the next frame
        time.sleep(wait)

    # release the VideoCapture object
    cap.release()

    # close frame display windows
    cv2.destroyAllWindows()

def basler_image_capture(duration, wait, image_folder):
    
    # create an instant camera object with the camera device found first.
    camera = pylon.InstantCamera(pylon.TlFactory.GetInstance().CreateFirstDevice())
    converter = pylon.ImageFormatConverter()
    
    # define the format
    converter.OutputPixelFormat = pylon.PixelType_BGR8packed
    converter.OutputBitAlignment = pylon.OutputBitAlignment_MsbAligned
    
    # open the camera and configure
    camera.Open()
    camera.UserSetSelector.SetValue("Default")
    camera.UserSetLoad.Execute()
    camera.GainAuto.SetValue("Off")
    camera.ExposureAuto.SetValue("Off")
    
    # configure auto exposure function if prferred
    # camera.AutoFunctionProfile.SetValue("MinimizeGain")
    # camera.AutoExposureTimeLowerLimit.SetValue(1000.0)
    # camera.AutoExposureTimeUpperLimit.SetValue(8000.0)
    
    camera.GainSelector.SetValue("All")
    camera.Gain.SetValue(0.00000)
    camera.ExposureTime.SetValue(8000.0)
    camera.PixelFormat.SetValue("RGB8")
    camera.LightSourcePreset.SetValue("Daylight5000K")

    # get the start time
    start_time = datetime.now()

    while (datetime.now() - start_time).total_seconds() < duration:
        print(datetime.now())
    
        # grab an image
        grabResult = camera.GrabOne(5000, pylon.TimeoutHandling_ThrowException)
                
        if grabResult.GrabSucceeded():
    
            # format the frame
            image = converter.Convert(grabResult)
            frame = image.GetArray()
        else:
            print("Failed to grab image.")
            frame = None

        # check if frame is not None before writing to file
        if frame is not None:
            # get the current timestamp and format it as a string
            timestamp = datetime.now().strftime('%H%M%S_%f')
            # create the full file path
            file_path = os.path.join(image_folder, f"{timestamp}.png")
            # check if the directory exists and create it if necessary
            directory = os.path.dirname(file_path)
            if not os.path.exists(directory):
                os.makedirs(directory)
            
            cv2.imwrite(file_path, frame, [cv2.IMWRITE_PNG_COMPRESSION, 0])
        else:
            print("No frame to write to file.")

        # wait for 'wait' seconds before capturing the next frame
        time.sleep(wait)
    
    # release grabResult resources
    grabResult.Release()
    
    # close the camera 
    camera.StopGrabbing()

# define a function to read the config file
def read_config(config_file_path):
  config = configparser.ConfigParser()
  try:
    config.read(config_file_path)
  except Exception as e:
      print(f"Error reading config file: {e}")
      return None
  try:
    duration = config["DEFAULT"]["duration"].split('#')[0].strip()
    wait = config["DEFAULT"]["wait"].split('#')[0].strip()
    sensor = config["DEFAULT"]["sensor"].split('#')[0].strip()
    data_path = config["DEFAULT"]["data_path"].split('#')[0].strip()
  except (KeyError, ValueError):
    print(f"Error retrieving values from config file.")
    return None
  return {"duration": duration, "wait": wait, "sensor": sensor, "data_path": data_path}    

### capture frames for 'duration' seconds and pause for 'wait' seconds between frame grabs
if __name__ == "__main__":
  
  # get the cmd line argument (run_ID)
  if len(sys.argv) > 1:
    run_ID = sys.argv[1]
  else:
    print("Run ID not provided")
    sys.exit()
  
  # read config file
  config_file_path = r"C:\Users\David_UoR\OneDrive - University of Reading\repos\FrameGrab\config.ini"
  config = read_config(config_file_path)
  print(f"config: {config}")
  duration = float(config["duration"])
  wait = float(config["wait"])
  sensor = config["sensor"]
  data_path = config["data_path"]
  frame_folder = (data_path+"\\\\"+run_ID)
  print(f"frame folder is {frame_folder}")
  if sensor == 'basler':
     basler_image_capture(duration, wait, frame_folder)
  elif sensor == 'builtin':
    builtin_image_capture(duration, wait, frame_folder)
  else:
    print(f'Invalid sensor: {sensor}. Please choose "basler" or "builtin" in the config file')
  
